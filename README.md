# KargoCard
This is source code of KargoCard android project, created for Tanya.

![preview][preview_image]

## Required
* Firebase [Firestore][firebase_firestore_doc_link]
* Firebase [Functions][firebase_functions_doc_link]
* [Server code][firebase_functions_project_link]
* Android API 21v or higher
* Java

[//]: # (LINKS)
[preview_image]: doc/preview_image.jpg
[firebase_firestore_doc_link]: https://firebase.google.com/docs/firestore
[firebase_functions_doc_link]: https://firebase.google.com/docs/functions
[firebase_functions_project_link]: https://gitlab.com/Samsuper/kargocard-functions
