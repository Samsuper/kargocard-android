package com.pro100gruz.kargocard.listeners;

import com.pro100gruz.kargocard.models.EntireData;

public interface OnGetEntireDataListener{
    void onSuccess(EntireData data);
    void onStart();
    void onFailure();
}
