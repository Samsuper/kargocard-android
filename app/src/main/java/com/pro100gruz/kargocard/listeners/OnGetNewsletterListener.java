package com.pro100gruz.kargocard.listeners;

import com.pro100gruz.kargocard.models.Newsletter;

import java.util.List;

public interface OnGetNewsletterListener{
    void onSuccess(List<Newsletter> list);
    void onFailed();
}
