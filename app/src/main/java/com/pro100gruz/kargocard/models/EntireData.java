package com.pro100gruz.kargocard.models;

public class EntireData {
    public String first_text;
    public String second_text;
    public String phone_one;
    public String phone_two;
    public String emailContact;
    public String emailSender;

    public String subject;
    public String sender;

    public EntireData(){}

    public EntireData(String first_text, String second_text,
                      String emailContact, String emailSender,
                      String phone_one, String phone_two,
                      String subject, String sender)
    {
        this.first_text = first_text;
        this.second_text = second_text;
        this.phone_one = phone_one;
        this.phone_two = phone_two;
        this.emailContact = emailContact;
        this.emailSender = emailSender;
        this.subject = subject;
        this.sender = sender;
    }
}
