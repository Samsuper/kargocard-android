package com.pro100gruz.kargocard.models;

public class Newsletter {

    public String id;
    public String Email;
    public boolean Status;

    public Newsletter(){}

    public Newsletter(String email, boolean status){
        this.Email = email;
        this.Status = status;
    }
}
