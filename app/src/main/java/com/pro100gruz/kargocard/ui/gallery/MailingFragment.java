package com.pro100gruz.kargocard.ui.gallery;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;
import com.pro100gruz.kargocard.listeners.OnWaitListener;
import com.pro100gruz.kargocard.models.EntireData;
import com.pro100gruz.kargocard.models.Newsletter;
import com.pro100gruz.kargocard.R;
import com.pro100gruz.kargocard.listeners.OnGetEntireDataListener;
import com.pro100gruz.kargocard.listeners.OnGetNewsletterListener;
import com.pro100gruz.kargocard.views.adapters.UnsentNewsletterAdapter;
import com.pro100gruz.kargocard.views.holders.UnsentNewsletterHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MailingFragment extends Fragment implements UnsentNewsletterHolder.OnNewsletterListener {

    private RecyclerView recyclerView;
    private Button sendBtn;
    private UnsentNewsletterAdapter unsentNewsletterAdapter;
    private String TAG = "NewsletterFragment";
    private List<Newsletter> newsletterList;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_mailing, container, false);
        recyclerView = (RecyclerView)root.findViewById(R.id.letters_recycleView);
        sendBtn = (Button)root.findViewById(R.id.newsletter_send_btn);
        unsentNewsletterAdapter = new UnsentNewsletterAdapter(getContext(), this);
        recyclerView.setAdapter(unsentNewsletterAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendAll();
            }
        });

        draw();

        return root;
    }

    @Override
    public void onDriverClicked(int position) {

    }

    private void sendAll(){
        fillEntireData(new OnGetEntireDataListener() {
            @Override
            public void onSuccess(EntireData data) {
                Log.w(TAG, "Количество писем: " + String.valueOf(newsletterList.size()));
                for(int i = 0; i < newsletterList.size(); ++i){
                    sendToEmail(newsletterList.get(i).Email, data);
                }

                Log.w(TAG, "Обновление писем: " + String.valueOf(newsletterList.size()));

                updateNewsletter(newsletterList, new OnWaitListener() {
                    @Override
                    public void Success() {
                        Log.w(TAG, "Переотрисовка");

                        reDraw();
                    }
                });
            }

            @Override
            public void onStart() {
                Toast.makeText(getActivity(), "Рассылка началась :",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure() {
                Toast.makeText(getActivity(), "Не удалось осуществить рассылку.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void reDraw(){
        unsentNewsletterAdapter.cleanData();
        draw();
    }

    private void draw(){
        readNewsletterByStatus(true, new OnGetNewsletterListener() {
            @Override
            public void onSuccess(List<Newsletter> list) {
                newsletterList = list;
                for(int i = 0; i< list.size(); ++i){
                    unsentNewsletterAdapter.insert(0, list.get(i));
                }
            }

            @Override
            public void onFailed() {
                Log.d(TAG, "Error per reading newsletters by status: true");
                Toast.makeText(getActivity(), "Не удалось подключиться к серверу.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }



    private void readNewsletterByStatus(final boolean status, final OnGetNewsletterListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("newsletter")
                .whereEqualTo("Status", status)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Newsletter> list = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Newsletter temp = new Newsletter();

                                temp.id = document.getId();
                                temp.Email = document.getString("Email");
                                temp.Status = document.getBoolean("Status");

                                list.add(temp);
                            }
                            listener.onSuccess(list);
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                            listener.onFailed();
                        }
                    }
                });
    }

    private void updateNewsletter(final List<Newsletter> list, final OnWaitListener listener){
        if(list.isEmpty()){
            return;
        }
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        for(int i =0; i < list.size(); ++i){
            db.collection("newsletter")
                    .document(list.get(i).id)
                    .update("Status", false)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        Log.w(TAG, "Обновление писем: успешное обновление");
                    }
                    else {
                        Log.w(TAG, "Обновление писем: ошибка обновления" + task.getException());
                    }
                    listener.Success();
                }
            });
        }
    }

    private void fillEntireData(final OnGetEntireDataListener listener){
        listener.onStart();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        try {
            DocumentReference docRef = db.collection("data").document("main");

            //Source source = Source.CACHE;
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()) {
                        DocumentSnapshot doc = task.getResult();
                        EntireData data = new EntireData();
                        data.first_text = doc.getString("first_text");
                        data.second_text = doc.getString("second_text");
                        data.phone_one = doc.getString("phone_one");
                        data.phone_two = doc.getString("phone_two");
                        data.emailContact = doc.getString("contact_email");
                        data.emailSender = doc.getString("sender_send");

                        data.subject = doc.getString("subject");
                        data.sender = doc.getString("sender");
                        listener.onSuccess(data);
                    }
                    else{
                        listener.onFailure();
                        Log.d(TAG, "Cached get failed: ", task.getException());
                    }
                }
            });

        } catch (NullPointerException e) {
            Toast.makeText(getActivity(), "Не удалось подключиться к серверу.",
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private Task<String> sendToEmail(String client_email, EntireData entire) {

        FirebaseFunctions functions = FirebaseFunctions.getInstance();

        // Create the arguments to the callable function, which are two integers
        Map<String, Object> data = new HashMap<>();
        data.put("first_text", entire.first_text);
        data.put("second_text", entire.second_text);
        data.put("phone_one", entire.phone_one);
        data.put("phone_two", entire.phone_two);
        data.put("our_email", entire.emailContact);

        data.put("client_email", client_email);
        data.put("subject", entire.subject);
        data.put("sender", entire.sender);

        // Call the function and extract the operation from the result
        return functions
                .getHttpsCallable("addMessage")
                .call(data)
                .continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        Map<String, Object> result = (Map<String, Object>) task.getResult().getData();
                        Log.w(TAG, "AddMessage sended: result - ");
                        return (String) result.get("text");
                    }
                });
    }

}
