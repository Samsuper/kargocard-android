package com.pro100gruz.kargocard.ui.home;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.pro100gruz.kargocard.R;
import com.pro100gruz.kargocard.listeners.OnGetEntireDataListener;
import com.pro100gruz.kargocard.models.EntireData;

public class HomeFragment extends Fragment {

    private EditText textFirst;
    private EditText textSecond;
    private EditText phoneOne;
    private EditText phoneTwo;
    private EditText sender;
    private EditText subject;
    private EditText senderEmail;
    private EditText contactEmail;
    private String TAG = "HomeFragment";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        textFirst = (EditText)root.findViewById(R.id.home_text_first);
        textSecond = (EditText)root.findViewById(R.id.home_text_second);
        phoneOne = (EditText)root.findViewById(R.id.home_text_telephone_one);
        phoneTwo = (EditText)root.findViewById(R.id.home_text_telephone_two);
        sender = (EditText)root.findViewById(R.id.home_text_sender);
        subject = (EditText)root.findViewById(R.id.home_text_subject);
        senderEmail = (EditText)root.findViewById(R.id.home_text_sender_email);
        contactEmail = (EditText)root.findViewById(R.id.home_text_contact_email);

        textFirst.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    updateNewsletter("first_text", textFirst.getText().toString());
                }
            }
        });

        textSecond.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    updateNewsletter("second_text", textSecond.getText().toString());
                }
            }
        });

        phoneOne.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    updateNewsletter("phone_one", phoneOne.getText().toString());
                }
            }
        });

        phoneTwo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    updateNewsletter("phone_two", phoneTwo.getText().toString());
                }
            }
        });

        sender.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    updateNewsletter("sender", sender.getText().toString());
                }
            }
        });

        subject.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    updateNewsletter("subject", subject.getText().toString());
                }
            }
        });

        senderEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    updateNewsletter("from_send", senderEmail.getText().toString());
                }
            }
        });

        contactEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    updateNewsletter("contact_email", contactEmail.getText().toString());
                }
            }
        });

        setupFields();
        return root;
    }

    private void setupFields(){

        getEntireData(new OnGetEntireDataListener() {
            @Override
            public void onSuccess(EntireData data) {
                textFirst.setText(data.first_text);
                textSecond.setText(data.second_text);
                phoneOne.setText(data.phone_one);
                phoneTwo.setText(data.phone_two);
                sender.setText(data.sender);
                subject.setText(data.subject);
                senderEmail.setText(data.emailSender);
                contactEmail.setText(data.emailContact);
            }

            @Override
            public void onStart() {
            }

            @Override
            public void onFailure() {
                Toast.makeText(getActivity(), "Не удалось подключиться к серверу.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateNewsletter(final String key, final Object value){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("data")
                    .document("main")
                    .update(key, value)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Log.w(TAG, "Обновление первичных данных: успешное обновление");
                            }
                            else {
                                Log.w(TAG, "Обновление первичных данных: ошибка обновления" + task.getException());
                            }
                        }
                    });
    }

    private void getEntireData(final OnGetEntireDataListener listener){
        listener.onStart();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        try {
            DocumentReference docRef = db.collection("data").document("main");

            //Source source = Source.CACHE;
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()) {
                        DocumentSnapshot doc = task.getResult();
                        EntireData data = new EntireData();
                        data.first_text = doc.getString("first_text");
                        data.second_text = doc.getString("second_text");
                        data.phone_one = doc.getString("phone_one");
                        data.phone_two = doc.getString("phone_two");
                        data.emailContact = doc.getString("contact_email");
                        data.emailSender = doc.getString("sender_email");

                        data.subject = doc.getString("subject");
                        data.sender = doc.getString("sender");
                        Log.w(TAG, doc.getData().toString());
                        listener.onSuccess(data);
                    }
                    else{
                        listener.onFailure();
                        Log.d(TAG, "Cached get failed: ", task.getException());
                    }
                }
            });

        } catch (NullPointerException e) {
            Toast.makeText(getActivity(), "Не удалось подключиться к серверу.",
                    Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

}
