package com.pro100gruz.kargocard.ui.slideshow;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.pro100gruz.kargocard.models.Newsletter;
import com.pro100gruz.kargocard.R;

import java.util.HashMap;
import java.util.Map;

public class NewsletterFragment extends Fragment {

    private EditText email;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_newsletter, container, false);

        email = (EditText)root.findViewById(R.id.adding_user_email);
        Button addBtn = (Button) root.findViewById(R.id.adding_button_add);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(email.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "Email пуст!",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                Newsletter newsletter = new Newsletter(email.getText().toString(), true);
                addNewsletter(newsletter);
                email.getText().clear();
            }
        });
        return root;
    }

    private void addNewsletter(Newsletter data){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String, Object> user = new HashMap<>();
        user.put("Email", data.Email);
        user.put("Status", data.Status);

        db.collection("newsletter")
                .add(user)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(getActivity(), "Пользователь добавлен!",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Ошибка при добавлении!",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
