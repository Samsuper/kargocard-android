package com.pro100gruz.kargocard.ui.thesaurus;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.pro100gruz.kargocard.models.Newsletter;
import com.pro100gruz.kargocard.R;
import com.pro100gruz.kargocard.listeners.OnGetNewsletterListener;
import com.pro100gruz.kargocard.ui.dialogs.MessageDialog;
import com.pro100gruz.kargocard.views.adapters.AllNewslettersAdapter;

import java.util.ArrayList;
import java.util.List;

public class ThesaurusFragment extends Fragment {

    private RecyclerView recyclerView;
    private EditText editText;
    private Button searchBtn;
    private AllNewslettersAdapter allNewslettersAdapter;
    private String TAG = "ThesaurusFragment";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_thesaurus, container, false);
        recyclerView = (RecyclerView)root.findViewById(R.id.all_letters_recycleView);
        editText = (EditText)root.findViewById(R.id.all_letters_search_text);
        searchBtn = (Button)root.findViewById(R.id.all_letter_search_btn);

        allNewslettersAdapter = new AllNewslettersAdapter(getContext());
        recyclerView.setAdapter(allNewslettersAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!editText.getText().toString().isEmpty()){
                    boolean state = checkList(editText.getText().toString());
                    //activate message menu
                    MessageDialog dialog = new MessageDialog("Поиск",
                            "Email найден:" +String.valueOf(state));
                    dialog.show(getChildFragmentManager(), "Tag");

                    editText.getText().clear();
                }
            }
        });

        getAllNewsletter(new OnGetNewsletterListener() {
            @Override
            public void onSuccess(List<Newsletter> list) {
                for(int i = 0; i <list.size(); ++i){
                    allNewslettersAdapter.insert(0, list.get(i));
                }
            }

            @Override
            public void onFailed() {
                Toast.makeText(getActivity(), "Не удалось подключиться к серверу.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        return root;
    }

    private void getAllNewsletter(final OnGetNewsletterListener listener){
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        db.collection("newsletter")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Newsletter> list = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Newsletter temp = new Newsletter();

                                temp.id = document.getId();
                                temp.Email = document.getString("Email");
                                temp.Status = document.getBoolean("Status");

                                list.add(temp);
                            }
                            listener.onSuccess(list);
                        } else {
                            Log.w("READ DATABASE:", "Error getting documents.", task.getException());
                            listener.onFailed();
                        }
                    }
                });
    }

    private boolean checkList(String match){
        for(int i = 0; i < allNewslettersAdapter.getItemCount(); ++i){
            if(allNewslettersAdapter.getDriverAtPosition(i).Email.equals(match)){
                return true;
            }
        }
        return false;
    }

}
