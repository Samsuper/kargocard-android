package com.pro100gruz.kargocard.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pro100gruz.kargocard.R;
import com.pro100gruz.kargocard.models.Newsletter;
import com.pro100gruz.kargocard.views.holders.AllNewslettersHolder;
import com.pro100gruz.kargocard.views.holders.UnsentNewsletterHolder;

import java.util.ArrayList;
import java.util.List;

public class AllNewslettersAdapter extends RecyclerView.Adapter<AllNewslettersHolder>{

    private List<Newsletter> newsletterList;
    private Context context;

    public AllNewslettersAdapter(Context context){
        this.context = context;

        this.newsletterList = new ArrayList<>();
    }

    @NonNull
    @Override
    public AllNewslettersHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.email_card, parent, false);
        return new AllNewslettersHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AllNewslettersHolder holder, int position) {
        holder.email.setText(newsletterList.get(position).Email);
    }

    @Override
    public int getItemCount() {
        return newsletterList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insert(int position, Newsletter letter) {
        newsletterList.add(position, letter);
        notifyItemInserted(position);
    }

    public void remove(Newsletter letter) {
        int position = newsletterList.indexOf(letter);
        newsletterList.remove(position);
        notifyItemRemoved(position);
    }

    public void removeAtPosition(int position){
        newsletterList.remove(position);
    }

    public Newsletter getDriverAtPosition(int position){
        return newsletterList.get(position);
    }
}
