package com.pro100gruz.kargocard.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pro100gruz.kargocard.models.Newsletter;
import com.pro100gruz.kargocard.R;
import com.pro100gruz.kargocard.views.holders.UnsentNewsletterHolder;

import java.util.ArrayList;
import java.util.List;

public class UnsentNewsletterAdapter extends RecyclerView.Adapter<UnsentNewsletterHolder>{

    private List<Newsletter> newsletterList;

    private UnsentNewsletterHolder.OnNewsletterListener newsletterListener;
    private Context context;

    public UnsentNewsletterAdapter(Context context, UnsentNewsletterHolder.OnNewsletterListener newsletterListener){
        this.context = context;
        this.newsletterListener = newsletterListener;
        this.newsletterList = new ArrayList<>();
    }

    @NonNull
    @Override
    public UnsentNewsletterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.email_card, parent, false);
        return new UnsentNewsletterHolder(v, newsletterListener);
    }

    @Override
    public void onBindViewHolder(@NonNull UnsentNewsletterHolder holder, int position) {
        holder.email.setText(newsletterList.get(position).Email);
    }

    @Override
    public int getItemCount() {
        return newsletterList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void insert(int position, Newsletter letter) {
        newsletterList.add(position, letter);
        notifyItemInserted(position);
    }

    public void remove(Newsletter letter) {
        int position = newsletterList.indexOf(letter);
        newsletterList.remove(position);
        notifyItemRemoved(position);
    }

    public void cleanData(){
        newsletterList.clear();
        notifyDataSetChanged();
    }

    public void removeAtPosition(int position){
        newsletterList.remove(position);
    }

    public Newsletter getDriverAtPosition(int position){
        return newsletterList.get(position);
    }
}