package com.pro100gruz.kargocard.views.holders;

import android.view.View;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.pro100gruz.kargocard.R;

public class AllNewslettersHolder extends RecyclerView.ViewHolder{

    public TextView email;

    public AllNewslettersHolder(View itemView) {
        super(itemView);
        CardView eCard = (CardView) itemView.findViewById(R.id.email_cardView);
        email = (TextView) itemView.findViewById(R.id.email_card_text);
    }
}
