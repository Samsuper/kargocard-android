package com.pro100gruz.kargocard.views.holders;

import android.view.View;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.pro100gruz.kargocard.R;

public class UnsentNewsletterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView email;

private OnNewsletterListener newsletterListener;

    public UnsentNewsletterHolder(View itemView, OnNewsletterListener listener) {
        super(itemView);
        CardView eCard = (CardView) itemView.findViewById(R.id.email_cardView);
        email = (TextView) itemView.findViewById(R.id.email_card_text);

        this.newsletterListener = listener;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
            newsletterListener.onDriverClicked(getAdapterPosition());
    }

    public interface OnNewsletterListener{
        void onDriverClicked(int position);
    }
}
